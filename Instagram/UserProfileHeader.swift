//
//  UserProfileHeader.swift
//  Instagram
//
//  Created by Arindam Santra on 27/01/19.
//  Copyright © 2019 Arindam Santra. All rights reserved.
//

import UIKit

class UserProfileHeader: UICollectionViewCell {
    
    
    var userDetails: User?{
        didSet{
            setupProfileImage()
            usernameLabel.text = userDetails?.userName
        }
    }
    
    
    
    
    let ivProfileImage: UIImageView = {
        let profileImage = UIImageView()
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        profileImage.contentMode = .scaleAspectFill
        profileImage.clipsToBounds = true
        profileImage.backgroundColor = .cyan
        profileImage.layer.cornerRadius = 88 / 2
        return profileImage
    }()
    
    
    
    
    
    
    let gridButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
        return button
    }()
    
    let listButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        return button
    }()
    
    let bookmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        return button
    }()
    
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "username"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let postsLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString(string: "11\n", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        attributedText.append(NSAttributedString(string: "posts", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]))
        label.attributedText = attributedText
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let followersLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        attributedText.append(NSAttributedString(string: "followers", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]))
        label.attributedText = attributedText
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let followingLabel: UILabel = {
        let label = UILabel()
        
        let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSAttributedString(string: "following", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]))
        
        label.attributedText = attributedText
        
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let editProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit Profile", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 3
        return button
    }()

    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(ivProfileImage)
        ivProfileImage.anchor(top: topAnchor, left: self.leftAnchor, right: nil, bottom: nil, paddingTop: 12, paddingBottom: 0, paddingLeft: 12, paddingRight: 0, height: 88, width: 88)
        
        setupBottomToolbar()

        self.addSubview(usernameLabel)
        usernameLabel.anchor(top: ivProfileImage.bottomAnchor, left: leftAnchor, right: rightAnchor, bottom: gridButton.topAnchor, paddingTop: 4, paddingBottom: 0, paddingLeft: 12, paddingRight: 12, height: 0, width: 0)
        
        setupUserStackView()
        
        
        self.addSubview(editProfileButton)
        editProfileButton.anchor(top: postsLabel.bottomAnchor, left: postsLabel.leftAnchor, right: rightAnchor, bottom: nil, paddingTop: 8, paddingBottom: 0, paddingLeft: 0, paddingRight: -8, height: 34, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    fileprivate func setupBottomToolbar(){
        let topDeviderView = UIView()
        topDeviderView.backgroundColor = UIColor.lightGray
        topDeviderView.translatesAutoresizingMaskIntoConstraints = false
        
        
        let bottomDevider = UIView()
        bottomDevider.backgroundColor = UIColor.lightGray
        bottomDevider.translatesAutoresizingMaskIntoConstraints = false
        
        
    let stackView = UIStackView(arrangedSubviews: [gridButton,listButton,bookmarkButton])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(stackView)
    self.addSubview(topDeviderView)
    self.addSubview(bottomDevider)
        
    topDeviderView.anchor(top: stackView.topAnchor, left: leftAnchor, right: rightAnchor, bottom: nil, paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 0.5, width: 0)
    stackView.anchor(top: nil, left: leftAnchor, right: rightAnchor, bottom: self.bottomAnchor, paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 50 , width: 0)
        
    bottomDevider.anchor(top: stackView.bottomAnchor, left: leftAnchor, right: rightAnchor, bottom: nil, paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 0.5, width: 0)

    }
    
    
    fileprivate func setupUserStackView(){
        let stackView = UIStackView(arrangedSubviews: [postsLabel,followersLabel,followingLabel])
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        self.addSubview(stackView)
        stackView.backgroundColor = .cyan
        stackView.anchor(top: topAnchor, left: ivProfileImage.rightAnchor, right: rightAnchor, bottom: nil, paddingTop: 12, paddingBottom: 0, paddingLeft: 6, paddingRight: 12, height: 50, width: 0)
    }
    
    
    
    func setupProfileImage(){
        guard let imageUrl = userDetails?.profileImageUrl else{return}
      guard  let url = URL(string: imageUrl) else{return}
        URLSession.shared.dataTask(with: url) { (result, response , error) in
            if let error = error {
                print("Failed to fetch profile image:", error)
                return
            }
            
            guard let result = result else { return }
            let image = UIImage(data: result)
            DispatchQueue.main.async {
                self.ivProfileImage.image = image
            }
        }.resume()
}
    
    
}
