//
//  ViewController.swift
//  Instagram
//
//  Created by Arindam Santra on 25/01/19.
//  Copyright © 2019 Arindam Santra. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage

import FirebaseAuth

class SignupController: UIViewController {
    
    
    let photoPlusButton: UIButton = {
        let plusButton = UIButton()
        plusButton.setImage(#imageLiteral(resourceName: "plus_photo"), for: .normal)
        plusButton.translatesAutoresizingMaskIntoConstraints = false
        plusButton.addTarget(self, action: #selector(handlePlusPhoto), for: .touchUpInside)
        return plusButton
    }()
    
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(textfieldValueChanged), for: .editingChanged)
        return tf
    }()
    
    let usernameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Username"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(textfieldValueChanged), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Password"
        tf.isSecureTextEntry = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(textfieldValueChanged), for: .editingChanged)
        return tf
    }()
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.isEnabled = false
        return button
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(photoPlusButton)
        setupViews()
        setupInputFields()
        signUpButton.addTarget(self, action: #selector(signupButtonTapp), for: .touchUpInside)
    }
    
    
    
    @objc func handlePlusPhoto() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func textfieldValueChanged(){
        
        let isFromValid = emailTextField.text?.isEmpty != true && usernameTextField.text?.isEmpty != true && passwordTextField.text?.isEmpty != true
        
        if isFromValid{
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = UIColor(red: 17/255, green: 154/255, blue: 237/255, alpha: 1)
        } else {
            signUpButton.isEnabled = false
            signUpButton.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        }
    }
    
    
    
    func setupViews(){
        NSLayoutConstraint.activate([
            photoPlusButton.heightAnchor.constraint(equalToConstant: 140),
            photoPlusButton.widthAnchor.constraint(equalToConstant: 140),
            photoPlusButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 40),
            photoPlusButton.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
            ])
    }
    
    
    fileprivate func setupInputFields() {
        let stackView = UIStackView(arrangedSubviews: [emailTextField, usernameTextField, passwordTextField, signUpButton])
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        stackView.anchor(top: photoPlusButton.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, bottom: nil, paddingTop: 40, paddingBottom: 0, paddingLeft: 40, paddingRight: -40, height: 200, width: 0)
        
    }
    
    
    @objc func signupButtonTapp(){
        
            guard let email = emailTextField.text, !email.isEmpty else { return }
            guard let username = usernameTextField.text, !username.isEmpty else { return }
            guard let password = passwordTextField.text, !password.isEmpty else { return }
            
            Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error: Error?) in
                
                if let err = error {
                    print("Failed to create user:", err)
                    return
                }
                
                print("Successfully created user:", user?.user.uid ?? "")
                
                guard let image = self.photoPlusButton.imageView?.image else { return }
                guard let uploadData = image.jpegData(compressionQuality: 0.3) else { return }
                let filename = NSUUID().uuidString
                
                let storageRef = Storage.storage().reference().child("profile_images").child(filename)
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, err) in
                    
                    if let err = err {
                        print("Failed to upload profile image:", err)
                        return
                    }
                    
                    // Firebase 5 Update: Must now retrieve downloadURL
                    storageRef.downloadURL(completion: { (downloadURL, err) in
                        if let err = err {
                            print("Failed to fetch downloadURL:", err)
                            return
                        }
                        
                        guard let profileImageUrl = downloadURL?.absoluteString else { return }
                        
                        print("Successfully uploaded profile image:", profileImageUrl)
                        
                        guard let uid = user?.user.uid else { return }
                        
                        let dictionaryValues = ["username": username, "profileImageUrl": profileImageUrl]
                        let values = [uid: dictionaryValues]
                        
                        Database.database().reference().child("users").updateChildValues(values, withCompletionBlock: { (err, ref) in
                            
                            if let err = err {
                                print("Failed to save user info into db:", err)
                                return
                            }
                            
                            print("Successfully saved user info to db")
                            
                            guard let maintabController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else{ return}
                            maintabController.setupViewControllers()
                            self.dismiss(animated: true, completion: {})
                        })
                    })
                })
                
            })
    }
}



extension SignupController: UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            photoPlusButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            photoPlusButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        photoPlusButton.layer.cornerRadius = photoPlusButton.frame.width/2
        photoPlusButton.layer.masksToBounds = true
        photoPlusButton.layer.borderColor = UIColor.black.cgColor
        photoPlusButton.layer.borderWidth = 3
        
        dismiss(animated: true, completion: nil)
        
        
        
        
        
    }
    
    
    
}



