//
//  DataModel.swift
//  Instagram
//
//  Created by Arindam Santra on 27/01/19.
//  Copyright © 2019 Arindam Santra. All rights reserved.
//

import Foundation


struct User {
    let userName: String
    let profileImageUrl: String
    
    init(dictionary: [String: Any]) {
        self.userName = dictionary["username"] as? String  ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
    }
}

